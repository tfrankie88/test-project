### Field Notes

• Used #1E1E1E instead of black as that was the color used in the prototype. This is easily changed in the SCSS file labeled primary-type.

• Decided after completing the the desktop layout that a 750px breakpoint was necessary. I understand that this is could have derived from my coding I just wanted to acknowledge this for review.

• First time working with SASS and BEM (essentially learned it for coding challenge). May be rough in and in need of refactoring in place but really loved working with it and will continue to get better at it.

• Utilized "Responsive Typography With SASS Maps" by Jonathan Suh for understanding typography breakpoints https://www.smashingmagazine.com/2015/06/responsive-typography-with-sass-maps/

• Thank you for this opportunity. This was a very fun task. The little quirks, and Barrel's standards challenged me to think outside of my normal conventions.

• I unfortunately ran out of time and didn't get to the modal views. I would be happy to explain how I would have accomplished this over our next call.
